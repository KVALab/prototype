﻿using System;

namespace Prototype
{
    /// <summary>
    /// Персонаж
    /// </summary>
    public class Character: IMyCloneable<Character>, ICloneable
    {
        public int Health { get; set; } = 100;
        public int Energy { get; set; } = 100;
        public int Power { get; set; } = 10;

        public Character(int health, int energy, int power)
        {
            Health = health;
            Energy = energy;
            Power = power;
        }

        public virtual Character MyClone()
            => new Character(Health, Energy, Power);
        
        public virtual object Clone()
            => MemberwiseClone();        
    }

}
