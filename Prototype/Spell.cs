﻿using System;

namespace Prototype
{
    public class Spell: IMyCloneable<Spell>, ICloneable
    {
        public string NameSpell { get; set; }
        public int PowerSpell{ get; set; }


        public Spell(string nameSpell, int powerSpell)
        {
            NameSpell = nameSpell;
            PowerSpell = powerSpell;
        }

        public Spell MyClone() 
            => new Spell( NameSpell, PowerSpell );
        
        public object Clone() 
            => MemberwiseClone();        
    }

}
