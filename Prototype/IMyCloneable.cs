﻿namespace Prototype
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}