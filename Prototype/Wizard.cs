﻿using System;

namespace Prototype
{
    /// <summary>
    /// Волшебник
    /// </summary>
    public class Wizard : Character, IMyCloneable<Wizard>, ICloneable
    {
        public Spell Spell { get; set; }
        
        public Wizard(int health, int energy, int power, Spell spell) :
            base(health, energy, power)
        {
            Spell = spell;
        }              

        public override Wizard MyClone()
            => new Wizard(Health, Energy, Power, Spell);

        public override object Clone()
        {
            var wizard = (Wizard)base.Clone();
            wizard.Spell = Spell.Clone() as Spell;
            return wizard;
        }

    }

}
