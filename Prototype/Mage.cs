﻿using System;

namespace Prototype
{
    /// <summary>
    /// Маг
    /// </summary>
    public class Mage : Wizard, IMyCloneable<Wizard>, ICloneable
    {
        public string SecretSpell { get; set; }
        public Mage(int health, int energy, int power, Spell spell, string secretspell) : 
            base(health, energy, power, spell)
        {
            SecretSpell = secretspell;
        }
        
        public override Mage MyClone()
            => new Mage(Health, Energy, Power, Spell, SecretSpell);

        public override object Clone()
            => (Mage)base.Clone();
    }

}
