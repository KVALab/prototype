﻿using System;

namespace Prototype
{
    /// <summary>
    /// Чародей
    /// </summary>
    public class Sorcerer : Wizard, IMyCloneable<Wizard>, ICloneable
    {
        public bool WithTeleporting { get; set; }
        public Sorcerer(int health, int energy, int power, Spell spell, bool withteleporting) :
            base(health, energy, power, spell)
        {
            WithTeleporting = withteleporting;
        }

        public override Sorcerer MyClone()
            => new Sorcerer(Health, Energy, Power, Spell, WithTeleporting);

        public override object Clone()
            => (Sorcerer)base.Clone();
    }

}
