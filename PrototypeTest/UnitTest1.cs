using Prototype;
using Xunit;

namespace PrototypeTest
{
    public class CaracterTest
    {
        [Fact]
        public void Clones_Is_Equals()
        {
            var sut = new Mage(100, 100, 50, new Spell("WaterArrow", 10), "Invisibility");
            var clone = sut.Clone() as Mage;
            Assert.True(WizardsEqual(sut, clone));
        }

        [Fact]
        public void Copies_are_not_equal_when_properties_change()
        {
            var sut = new Sorcerer(100, 100, 50, new Spell("Freeze", 12), true);
            var clone = sut.Clone() as Sorcerer;

            clone.Spell = new Spell("Fire", 12);

            Assert.NotSame(sut, clone);
            Assert.False(WizardsEqual(sut, clone));
        }

        private bool WizardsEqual(Wizard origin, Wizard copy)
        {
            return origin.Energy == copy.Energy &&
                   origin.Health == copy.Health &&
                   origin.Power == copy.Power &&
                   origin.Spell.NameSpell == copy.Spell.NameSpell &&
                   origin.Spell.PowerSpell == copy.Spell.PowerSpell;
        }
    }
}
